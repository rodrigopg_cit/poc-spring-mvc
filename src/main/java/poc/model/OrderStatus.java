package poc.model;

public enum OrderStatus {
    OPEN,
    BILLED,
    SHIPPED,
    CANCELLED
}
