package poc.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "T_ORDER_ITEM")
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long orderItemId;

    private String productId;
    private String productName;
    private Integer quantity;
    private BigDecimal orderValue;

    @JoinColumn(nullable = false)
    @ManyToOne(optional = false)
    private Order order;


    public OrderItem() {
    }

    public OrderItem(String productId, String productName, Integer quantity) {
        this.productId = productId;
        this.productName = productName;
        this.quantity = quantity;
    }

    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public BigDecimal getOrderValue() {
        return orderValue;
    }

    public void setOrderValue(BigDecimal orderValue) {
        this.orderValue = orderValue;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
            "orderItemId=" + orderItemId +
            ", productId='" + productId + '\'' +
            ", productName='" + productName + '\'' +
            ", quantity=" + quantity +
            '}';
    }
}
