package poc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import poc.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
