package poc.controller;

import poc.model.Order;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderController {

    @GetMapping
    public List<Order> findOrders() {
        // TODO
        return null;
    }

    @PostMapping
    public void createOrder(@PathVariable final Long orderId) {
        // TODO
    }

    @PutMapping
    public List<Order> cancelOrder(@PathVariable final Long orderId) {
        // TODO
        return null;
    }

}
